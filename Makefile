vcd: riscv-sim-tb.vcd

riscv-sim-tb.vvp: riscv-sim-tb.v riscv-sim.v
	iverilog -o riscv-sim-tb.vvp riscv-sim-tb.v riscv-sim.v

riscv-sim-tb.vcd: riscv-sim-tb.vvp
	vvp riscv-sim-tb.vvp

clean:
	rm *.vcd *.vvp
