// Simulation of a RISC-V Processor

// Definitions for Major Processor Opcodes
`define LOAD        5'b00000
`define LOAD_FP     5'b00001
`define CSTM_0      5'b00010
`define MSC_MEM     5'b00011
`define OP_IMM      5'b00100
`define AUIPC       5'b00101
`define OP_IMM32    5'b00110
`define IW48_0      5'b00111
`define STORE       5'b01000
`define STORE_FP    5'b01001
`define CSTM_1      5'b01010
`define AMO         5'b01011
`define OP          5'b01100
`define LUI         5'b01101
`define OP_32       5'b01110
`define IW64        5'b01111
`define MADD        5'b10000
`define MSUB        5'b10001
`define NMSUB       5'b10010
`define NMADD       5'b10011
`define OP_FP       5'b10100
`define RESERV_0    5'b10101
`define CSTM_2      5'b10110
`define IW48_1      5'b10111
`define BRANCH      5'b11000
`define JALR        5'b11001
`define RESERV_1    5'b11010
`define JAL         5'b11011
`define SYSTEM      5'b11100
`define RESERV_2    5'b11101
`define CSTM_3      5'b11110
`define IW_80       5'b11111

// Definitions for minor opcode fields

// funct3 Fields

`define F3_JALR 3'b000

`define F3_EQ   3'b000
`define F3_NE   3'b001
`define F3_LT   3'b100
`define F3_GE   3'b101
`define F3_LTU  3'b110
`define F3_GEU  3'b111

`define F3_LB   3'b000
`define F3_LH   3'b001
`define F3_LW   3'b010
`define F3_LBU  3'b100
`define F3_LHU  3'b101

`define F3_SB   3'b000
`define F3_SH   3'b001
`define F3_SW   3'b010

`define F3_ADDI 3'b000
`define F3_SLTI 3'b010
`define F3_SLTIU 3'b011
`define F3_XORI 3'b100
`define F3_ORI  3'b110
`define F3_ANDI 3'b111

`define F3_SLLI 3'b001
`define F3_SRI  3'b101

`define F3_ADD  3'b000
`define F3_SUB  3'b000
`define F3_SLL  3'b001
`define F3_SLT  3'b010
`define F3_SLTU 3'b011
`define F3_XOR  3'b100
`define F3_SR   3'b101
`define F3_OR   3'b110
`define F3_AND  3'b111

`define F3_FENCE    3'b000
`define F3_ECALL    3'b000
`define F3_EBREAK   3'b000

// funct7 Fields

//`define F7_SLLI 7'b0000000
`define F7_SRLI 7'b0000000
`define F7_SRAI 7'b0100000
`define F7_ADD  7'b0000000
`define F7_SUB  7'b0100000
//`define F7_SLL  7'b0000000
`define F7_SLT  7'b0000000
`define F7_SLTU 7'b0000000
`define F7_XOR  7'b0000000
`define F7_SRL  7'b0000000
`define F7_SRA  7'b0100000
`define F7_OR   7'b0000000
`define F7_AND  7'b0000000


// Definitions for Instruction Fields
`define INST_OP     6:2   // major opcode
`define INST_F3     14:12 // minor F3 opcode
`define INST_F7     31:25 // minor F7 opcode

`define INST_RD     11:7  // rd
`define INST_RS1    19:15 // rs1
`define INST_RS2    24:20 // rs2

// Immediate bit fields
`define IMM_ISBJ_10_5   30:25
`define IMM_IS_11       31
`define IMM_IJ_4_1      24:21
`define IMM_I_0         20
`define IMM_SB_4_1      11:8
`define IMM_S_0         7
`define IMM_B_11        7
`define IMM_B_12        31
`define IMM_UJ_19_12    19:12
`define IMM_U_30_20     30:20
`define IMM_J_11        20
`define IMM_J_10_1      30:21
`define IMM_J_20        31

`define IMM_MSB     31  // Immediate's sign bit

`define IMM_SHAMT   24:20 // Shift amount

// Definition of Zero for the processor (Usually data width)
`define ZERO        32'd0

module RISC_V (clk);

    input  clk;

    // Main register file
    reg [31:0] rf[0:31];

    // Program Counter
    reg [31:0] pc;

    // Main memory (8KB)
    reg [7:0] m[0:8191];
    
    // Instruction Register
    reg [31:0] instr;

    // Instruction fields
    wire [4:0] rd  =    instr[`INST_RD];
    wire [4:0] rs1 =    instr[`INST_RS1];
    wire [4:0] rs2 =    instr[`INST_RS2];

    // Immediate fields
    wire [31:0] imm_i = { {21{instr[`IMM_MSB]}}, instr[`IMM_ISBJ_10_5],
                        instr[`IMM_IJ_4_1], instr[`IMM_I_0] };
    wire [31:0] imm_s = { {21{instr[`IMM_MSB]}}, instr[`IMM_ISBJ_10_5],
                        instr[`IMM_SB_4_1], instr[`IMM_S_0] };
    wire [31:0] imm_b = { {20{instr[`IMM_MSB]}}, instr[`IMM_B_11],
                        instr[`IMM_ISBJ_10_5], instr[`IMM_SB_4_1], 1'b0 };
    wire [31:0] imm_u = { instr[`IMM_MSB], instr[`IMM_U_30_20],
                        instr[`IMM_UJ_19_12], 12'b0 };
    wire [31:0] imm_j = { {12{instr[`IMM_MSB]}}, instr[`IMM_UJ_19_12],
                        instr[`IMM_J_11], instr[`IMM_ISBJ_10_5],
                        instr[`IMM_IJ_4_1], 1'b0 };

    wire [4:0] shamt = instr[`IMM_SHAMT];

    // Next program counter
    wire [31:0] pc4 = pc + 4;

    // Sets the zero register to zero
    always @(negedge clk) begin
        
        rf[0] <= `ZERO;

    end

    always @(posedge clk) begin

        // Fetch Instruction Register
        instr[7:0]   = m[pc];
        instr[15:8]  = m[pc+1];
        instr[23:16] = m[pc+2];
        instr[31:24] = m[pc+3];

        // Increment Program Counter
        pc = pc4;
        
        // Delay for case logic
        #5

        // Instruction Execute
        case ( instr[`INST_OP] )
            // RV32I, Zifencei, Zicsr, RV32M
            `LUI:   rf[rd] = imm_u;
            `AUIPC: rf[rd] = imm_u + pc-4;
            `JAL:   begin
                        rf[rd] = pc4;
                        pc = imm_j + pc-4; 
                    end
            `JALR:  case ( instr[`INST_F3] )
                        `F3_JALR:   begin
                                        rf[rd] = pc4;
                                        pc = {rf[rs1][31:1], 1'b0}
                                              + {imm_i[31:1], 1'b0};
                                    end
                        default: begin $display("Invalid JALR"); $stop; end
                    endcase
            `BRANCH:case ( instr[`INST_F3] )
                        `F3_EQ: if (rf[rs1] == rf[rs2])
                                    pc = pc-4 + imm_b;
                        `F3_NE: if (rf[rs1] != rf[rs2])
                                    pc = pc-4 + imm_b;
                        `F3_LT: if ($signed(rf[rs1]) < $signed(rf[rs2]))
                                    pc = pc-4 + imm_b;
                        `F3_GE: if ($signed(rf[rs1]) >= $signed(rf[rs2]))
                                    pc = pc-4 + imm_b;
                        `F3_LTU:if (rf[rs1] < rf[rs2])
                                    pc = pc-4 + imm_b;
                        `F3_GEU:if (rf[rs1] >= rf[rs2])
                                    pc = pc-4 + imm_b;
                        default: begin $display("Invalid BRANCH"); $stop; end
                    endcase
            `LOAD:  case ( instr[`INST_F3] )
                        `F3_LB: rf[rd] = { {24{m[rf[rs1] + imm_i][7]}}, 
                                          m[rf[rs1] + imm_i]};
                        `F3_LH: rf[rd] = { {16{m[rf[rs1] + imm_i + 1][7]}}, 
                                          m[rf[rs1] + imm_i + 1],
                                          m[rf[rs1] + imm_i]};
                        `F3_LW: rf[rd] = { m[rf[rs1] + imm_i + 3], 
                                          m[rf[rs1] + imm_i + 2],
                                          m[rf[rs1] + imm_i + 1],
                                          m[rf[rs1] + imm_i]};
                        `F3_LBU:rf[rd] = { 24'b0, m[rf[rs1] + imm_i]};
                        `F3_LHU:rf[rd] = { 16'b0, m[rf[rs1] + imm_i + 1],
                                          m[rf[rs1] + imm_i]};
                        default: begin $display("Invalid LOAD"); $stop; end
                    endcase
            `STORE: case ( instr[`INST_F3] )
                        `F3_SB: m[rf[rs1] + imm_s] = rf[rs2][7:0];
                        `F3_SH: begin 
                                    m[rf[rs1] + imm_s] = rf[rs2][7:0];
                                    m[rf[rs1] + imm_s + 1] = rf[rs2][15:8];
                                end
                        `F3_SW: begin 
                                    m[rf[rs1] + imm_s] = rf[rs2][7:0];
                                    m[rf[rs1] + imm_s + 1] = rf[rs2][15:8];
                                    m[rf[rs1] + imm_s + 2] = rf[rs2][23:16];
                                    m[rf[rs1] + imm_s + 3] = rf[rs2][31:24];
                                end
                        default: begin $display("Invalid STORE"); $stop; end
                    endcase
            `OP_IMM:case ( instr[`INST_F3] )
                        `F3_ADDI:   rf[rd] = rf[rs1] + imm_i;
                        `F3_SLTI: if ( $signed(rf[rs1]) < $signed(imm_i) )
                                    rf[rd] = 32'b1;
                                  else
                                    rf[rd] = 32'b0;
                        `F3_SLTIU:if ( rf[rs1] < imm_i )
                                    rf[rd] = 32'b1;
                                  else
                                    rf[rd] = 32'b0;
                        `F3_XORI:   rf[rd] = rf[rs1] ^ imm_i;
                        `F3_ORI:    rf[rd] = rf[rs1] | imm_i;
                        `F3_ANDI:   rf[rd] = rf[rs1] & imm_i;
                        `F3_SLLI:   rf[rd] = rf[rs1] << shamt;
                        `F3_SRI:case ( instr[`INST_F7] )
                                    `F7_SRLI: rf[rd] = rf[rs1] >> shamt;
                                    `F7_SRAI: rf[rd] = rf[rs1] >>> shamt;
                                    default: begin $display("SRI"); $stop; end
                                endcase
                        default: begin $display("Invalid OP_IMM"); $stop; end
                    endcase
            `OP:    case ( instr[`INST_F3] )
                        `F3_ADD:case ( instr[`INST_F7] )
                                    `F7_ADD:    rf[rd] = rf[rs1] + rf[rs2];
                                    `F7_SUB:    rf[rd] = rf[rs1] - rf[rs2];
                                    default: begin $display("ADD"); $stop; end
                                endcase
                        `F3_SLL:    rf[rd] = rf[rs1] << rf[rs2][4:0];
                        `F3_SLT: if ( $signed(rf[rs1]) < $signed(rf[rs2]) )
                                     rf[rd] = 32'b1;
                                 else
                                     rf[rd] = 32'b0;
                        `F3_SLTU:if ( rf[rs1] < rf[rs2] )
                                     rf[rd] = 32'b1;
                                 else
                                     rf[rd] = 32'b0;
                        `F3_XOR:    rf[rd] = rf[rs1] ^ rf[rs2];
                        `F3_SR: case ( instr[`INST_F7] )
                                    `F7_SRL: rf[rd] = rf[rs1] >> rf[rs2][4:0];
                                    `F7_SRA: rf[rd] = rf[rs1] >>> rf[rs2][4:0];
                                    default: begin $display("SR"); $stop; end
                                endcase
                        `F3_OR:     rf[rd] = rf[rs1] | rf[rs2];
                        `F3_AND:    rf[rd] = rf[rs1] & rf[rs2];
                        default: begin $display("Invalid OP"); $stop; end
                    endcase
            `MSC_MEM:   case ( instr[`INST_F3] )
                            `F3_FENCE: ; // NOP
                            default: begin $display("MSC_MEM"); $stop; end
                        endcase
            `SYSTEM:    case ( instr[`INST_F3] )
                            `F3_ECALL: begin $display("ECALL"); $stop; end
                            `F3_EBREAK: begin $display("EBREAK"); $stop; end
                            default: begin $display("SYSTEM"); $stop; end
                        endcase

            /* Not Yet implemented (Invalid)
            // RV64I, RV64M
            `OP_32:
            `OP_IMM32:

            // RV32A, RV64A
            `AMO:

            // RV32F, RV64F, RV32D, RV64D, RV32Q, RV64Q
            `LOAD_FP:
            `STORE_FP:
            `MADD:
            `MSUB:
            `NMSUB:
            `NMADD:
            `OP_FP:

            // Opcode Extentions
            `IW48:
            `IW64:
            `IW48_0:
            `IW80:
            `IW48_1:

            // Custom, Reserved
            `CSTM_0:
            `CSTM_1:
            `RESERV_0:
            `CSTM_2:
            `RESERV_1:
            `RESERV_2:
            `CSTM_3:
            */

            default: begin $display("Invalid Major Opcode"); $stop; end
        endcase

    end

endmodule
