// Testing module for RISC-V processor

`define ZERO 32'd0

module riscv_test ();
    
    reg clk;

    RISC_V cpu(clk);

    integer j;

    initial begin
        
        $dumpfile ("riscv-sim-tb.vcd");
        $dumpvars (0, riscv_test);
        
        for(j=0; j < 32; j++) begin
            $dumpvars(1, cpu.rf[j]);
        end

        #1 cpu.pc = 0;
        cpu.rf[0] = `ZERO;
        cpu.rf[1] = `ZERO;
        cpu.rf[2] = `ZERO;
        cpu.rf[3] = `ZERO;
        cpu.rf[4] = `ZERO;
        cpu.rf[5] = `ZERO;
        cpu.rf[6] = `ZERO;
        cpu.rf[7] = `ZERO;
        cpu.rf[8] = `ZERO;
        cpu.rf[9] = `ZERO;
        cpu.rf[10] = `ZERO;
        cpu.rf[11] = `ZERO;
        cpu.rf[12] = `ZERO;
        cpu.rf[13] = `ZERO;
        cpu.rf[14] = `ZERO;
        cpu.rf[15] = `ZERO;
        cpu.rf[16] = `ZERO;
        cpu.rf[17] = `ZERO;
        cpu.rf[18] = `ZERO;
        cpu.rf[19] = `ZERO;
        cpu.rf[20] = `ZERO;
        cpu.rf[21] = `ZERO;
        cpu.rf[22] = `ZERO;
        cpu.rf[23] = `ZERO;
        cpu.rf[24] = `ZERO;
        cpu.rf[25] = `ZERO;
        cpu.rf[26] = `ZERO;
        cpu.rf[27] = `ZERO;
        cpu.rf[28] = `ZERO;
        cpu.rf[29] = `ZERO;
        cpu.rf[30] = `ZERO;
        cpu.rf[31] = `ZERO;

        for (j=0; j<8192; j=j+1)
            cpu.m[j] = 8'h0;

        #1 $readmemh("./tests/test2.dat", cpu.m);

        for (j=0; j<32; j=j+1)
            $display("Mem at: %d, %h", j, cpu.m[j]);

        #120 $finish;
    end

    always begin
        
        #5 clk = 1'b0;
        #5 clk = 1'b1;

        $display("Register Contents:");
        $display("  r0 - %h", cpu.rf[0]);
        $display("  r1 - %h", cpu.rf[1]);
        $display("  r2 - %h", cpu.rf[2]);
        $display("  r3 - %h", cpu.rf[3]);
        $display("  r4 - %h", cpu.rf[4]);
        $display("  r5 - %h", cpu.rf[5]);
        $display("  r6 - %h", cpu.rf[6]);
        $display("  r7 - %h", cpu.rf[7]);

    end

endmodule
